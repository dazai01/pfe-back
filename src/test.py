# from owlready2 import *
# import distance


# onto = get_ontology('file:///home/dazai/upload/these/DossierMahran/server/resources/AlignementActivitySectorF1.owl').load()
# for individual in onto.individuals():
#     if 'Password' == individual.name:
#         asset = individual

# print(asset.name)

# onto = get_ontology('file://resources/AlignementRiskTestF1.owl').load()

# for c in onto.classes():
#     # print(c.name)
#     if c.name == 'Threat':
#         print(c.instances()[0].threatens_asset)

from flask import Flask, render_template
from flask_socketio import SocketIO
from flask import request

app = Flask(__name__)
# app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, transports=['websocket'])

@socketio.on('connect')
def connect():
    print('connected')
    print(request.sid)

if __name__ == '__main__':
    socketio.run(app, debug=True)