from owlready2 import *
import os
import distance

from semantic_web.models import static_models


def getAllDomains() -> list:
    cwd = os.getcwd()
    domainOntology = get_ontology(
        "file:///" + cwd + "/../resources/AlignementActivitySectorF1.owl").load()
    print(list(domainOntology.Domain.instances()))
    return list(set(domainOntology.Domain.instances()).difference(set(domainOntology.ActivitySector.instances())))
    # domains = []
    # for element in set(domainOntology.Domain.instances()).difference(set(domainOntology.ActivitySector.instances())):
    #     domains.append(element.name)
    # return domains


def getSubDomains(domain: str) -> list:
    domains = getAllDomains()
    filteredDomain = list(filter(lambda e: e.name == domain, domains))[0]
    return filteredDomain.isComposedOf


def getAssets(subDomain: str) -> list:
    cwd = os.getcwd()
    domainOntology = get_ontology(
        "file:///" + cwd + "/../resources/AlignementActivitySectorF1.owl").load()
    filteredSubDomain = list(filter(
        lambda e: e.name == subDomain, domainOntology.ActivitySector.instances()))[0]
    print('-----------------------')
    print(filteredSubDomain.name)
    print(filteredSubDomain.hasAsset)
    return filteredSubDomain.hasAsset


def addAssetToRiskOntology(assetName: str):
    cwd = os.getcwd()
    onto = get_ontology('file:///' + cwd +
                        '/../resources/AlignementActivitySectorF1.owl').load()
    for individual in onto.individuals():
        if assetName == individual.name:
            asset = individual

    onto = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRiskTestF1.owl').load()

    for c in onto.classes():
        if distance.levenshtein(asset.is_a[0].name, c.name) <= 1:
            eval('c(asset.name)')
            onto_path.append(cwd + '/../resources/')
            onto.save()

def getThreats(asset_name: str):
    cwd = os.getcwd()
    onto1 = get_ontology('file:///' + cwd +
                        '/../resources/AlignementActivitySectorF1.owl').load()
    onto2 = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRiskTestF1.owl').load()
    # asset = None
    # for i in onto1.individuals():
    #     if i.name == asset_name:
    #         asset = i
    # threats = []
    # for i in onto2.individuals():
    #     if i.threatens_securityProperty != []:
    #         for t in i.threatens_securityProperty:
    #             if t.name in [s.name[:-2] for s in asset.requireSecurityProperties if s.name.endswith('-H')]:
    #                 threats.append(i.name)
    # return list(set(threats))
    return static_models.threats.get(asset_name)

def getVulnerabilities(threat: str):
    cwd = os.getcwd()
    onto = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRiskTestF1.owl').load()
    for c in onto.classes():
        if c.name == 'Threat':
            for th in c.instances():
                if th.name == threat:
                    return list(set([v.name for v in th.exploits]))


def getRisks(vulnerability: str):
    cwd = os.getcwd()
    onto = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRiskTestF1.owl').load()
    for c in onto.classes():
        if c.name == 'Vulnerability':
            for v in c.instances():
                if v.name == vulnerability:
                    return list(set([r.name for r in v.generates]))


def getImpacts(risk: str):
    cwd = os.getcwd()
    onto = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRiskTestF1.owl').load()
    for c in onto.classes():
        if c.name == 'Risk':
            for r in c.instances():
                if r.name == risk:
                    return list(set([i.name for i in r.involve]))


def getSecurityRequirements(vulnerability: str, risk: str):
    res_dict = static_models.security_requirements.get(vulnerability)
    if res_dict is None:
        res_dict = static_models.security_requirements.get('default')
    res_dict = res_dict.get(risk)
    if res_dict is None:
        res_dict = static_models.security_requirements.get('default')
        return res_dict.get(risk)
    return res_dict

def getCounterMeasure(requirement: str):
    cwd = os.getcwd()
    onto = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRequirmentTestF1.owl').load()
    for c in onto.classes():
        if c.name == 'Security_requirement':
            for s in c.instances():
                if s.name == requirement:
                    return list(set([e.name for e in s.performed_by]))


def getRequirementsByPhase():
    cwd = os.getcwd()
    onto = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRequirmentTestF1.owl').load()
    onto1 = get_ontology('file:///' + cwd +
                        '/../resources/AlignementRiskTestF1.owl').load()
    req = []
    v = static_models.phase['vulnerabilities']
    r = static_models.phase['risk']
    requirements = []
    for risk in r:
        for k, v in static_models.security_requirements.items():
            for i in list(v.keys()):
                if i == risk:
                    print(i, list(v.get(i)), risk, sep='|')
                    requirements.extend(v.get(i))
    requirements = list(set(requirements))
    # for e in v:
    #     for f in r:
    #         if static_models.security_requirements.get(e) is not None:
    #             if static_models.security_requirements.get(e).get(f) is not None:
    #                 req.append(static_models.security_requirements.get(e).get(f))
    # req1 = []
    # for l in req:
    #     for e in l:
    #         req1.append(e)
    # req1 = list(set(req1))
    req = {}
    # static_models.phase['requirements'] = req1
    static_models.phase['requirements'] = requirements
    for e in static_models.phase['requirements']:
        t = set()
        for i in onto.individuals():
            if e == i.name:
                for a in i.is_a:
                    print('----------------------------')
                    print(a.name)
                    print('----------------------------')
                    t.add(static_models.req_type[a.name])
        req[e] = list(t)
    static_models.phase['requirements'] = req
    req = {}
    vul = {}
    for e in static_models.phase['vulnerabilities']:
        t = set()
        for i in onto1.individuals():
            if e == i.name:
                for a in i.is_a:
                    t.add(static_models.vulnerability_type[a.name])
        vul[e] = list(t)
    static_models.phase['vulnerabilities'] = vul
    print(static_models.phase)
                
    # counter_measure_list = []
    # print(static_models.by_phase.get('requirements'))
    # for e in static_models.by_phase.get('requirements'):
    #     print(getCounterMeasure(e))
    #     counter_measure_list.append({e: getCounterMeasure(e)})
    # print(counter_measure_list)
    # if len(counter_measure_list) == 1:
    #     static_models.by_phase['design'] = counter_measure_list[0]
    # elif len(counter_measure_list) == 2:
    #     static_models.by_phase['design'] = counter_measure_list[0]
    #     static_models.by_phase['analysis'] = counter_measure_list[1]
    # else:
    #     static_models.by_phase['design'] = counter_measure_list[0]
    #     static_models.by_phase['analysis'] = counter_measure_list[1]
    #     static_models.by_phase['implementation'] = counter_measure_list[2]


def applySWRLRule(rule_as_str: str):
    rule = Imp()
    rule.set_as_rule(rule_as_str)
    sync_reasoner()


def generateRule1() -> str:
    return 'ActivitySector(?a) ^ Asset.Data(?d) ^ Security.property(?z) ^ hasLevel(?z, high) ^ threat(?t) ^ threaten(?t, ?z) -> threaten(?t, ?d)'


def generateRule2() -> str:
    return 'Asset.Data(?d) ^ Security.property(?z) ^ hasLevel(?z, ?high) -> Protect(?c, ?d) ^ Protect(?c, ?z)'
