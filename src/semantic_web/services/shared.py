from owlready2 import *



def map_to_identity(entry: list[str]) -> list:
    return list(map(lambda e: {"name": e, "_id": e}, entry))
