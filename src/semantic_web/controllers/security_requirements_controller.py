import json
import uuid
from flask import request
from semantic_web import app, db
from semantic_web.models import static_models, models
from flask_cors import cross_origin
from semantic_web.services import ontologies_service, shared


@app.route("/security-requirements/<vulnerability>/<risk>", methods=['GET'])
@cross_origin()
def get_security_requirements(vulnerability, risk):
    risk = request.view_args['risk']
    vulnerability = request.view_args['vulnerability']
    security_requirements = ontologies_service.getSecurityRequirements(vulnerability, risk)
    static_models.by_phase['requirements'] = security_requirements
    static_models.phase['requirements'] = security_requirements
    static_models.way['requirements'] = security_requirements
    response = shared.map_to_identity(security_requirements)
    return app.response_class(response=json.dumps({'items': response}), status=200, mimetype='Application/json')


@app.route("/security-requirements/<requirement>", methods=['GET'])
@cross_origin()
def get_counter_measure(requirement):
    requirement = request.view_args['requirement']
    counter_measures = ontologies_service.getCounterMeasure(requirement)
    static_models.way['counter'] = counter_measures
    static_models.phase['counter'] = counter_measures
    print('--------------------------')
    print(static_models.way)
    response = shared.map_to_identity(counter_measures)
    return app.response_class(response=json.dumps({'items': response}), status=200, mimetype='Application/json')


@app.route("/security-requirements/by_phase", methods=['GET'])
@cross_origin()
def get_by_phase():
    ontologies_service.getRequirementsByPhase()
    # return app.response_class(response=json.dumps({'items': [static_models.phase]}), status=200, mimetype='Application/json')
    return app.response_class(response=json.dumps({
    "items": [
        {
            "vulnerabilities": {
                "Failure_To_Restrict_URL_Access": [
                    "design"
                ],
                "Insecure_direct_object_references": [
                    "implementation"
                ],
                "Overly_Informative_Error_Messages": [
                    "implementation",
                    "analysis"
                ],
                "Insufficient_Authorization": [
                    "analysis"
                ],
                "Information_Exposure_Through_Error_Message": [
                    "implementation",
                    "analysis"
                ],
                "Incomplete_Blacklist": [
                    "implementation"
                ],
                "Incorrect_Behavior_Order": [
                    "implementation"
                ]
            },
            "risk": [
                "Access_Unauthorized_Page",
                "Accessing_Account_Information",
                "Access_Control_Bypass",
                "Access_Unauthorized_Data"
            ],
            "requirements": {
                "syntactical_input_validation": [
                    "implementation"
                ],
                "Identity_verfication_by_username/password": [],
                "protect_accessible_object": [
                    "implementation"
                ],
                "Sufficient_verification_of_identity_at_both_ends_of_communication_channel": [],
                "protect_user/url_redirection": [
                    "design"
                ],
                "restrict_URL_access": [
                    "design"
                ],
                "IP_address_verification": [],
                "protecting_session_hijacking": [
                    "analysis"
                ],
                "prevent_URL_redirection_to_untrusted_site": [
                    "implementation"
                ],
                "Use a list of approved URLs": [],
                "need_encryption_to_store_private_data": [
                    "implementation"
                ],
                "need_access_control": [
                    "analysis"
                ],
                "use_input_validation_framework": [
                    "design"
                ],
                "use_possible_permission_on_file_access": [
                    "design"
                ],
                "need_integrity": [
                    "analysis"
                ],
                "semantic_input_validation": [
                    "implementation"
                ],
                "reliable_communication": []
            },
            "counter": [
                "discretionary_access_control",
                "re-authentication_for_sensitive_features",
                "error_codes_and_URLs",
                "indirect_object_references"
            ]
        }
    ]
}), status=200, mimetype='Application/json')


@app.route("/overview", methods=['GET'])
@cross_origin()
def overview():
    return app.response_class(response=json.dumps({'items': [static_models.way]}), status=200, mimetype='Application/json')


@app.route("/config", methods=['POST'])
@cross_origin()
def save_configuration():
    # body = request.get_json()
    # print(body)
    config = models.Configuration(id=uuid.uuid4().hex, domain=static_models.way['domain'], sub_domain=static_models.way['subDomain'],
                                  asset=static_models.way['asset'], threat=static_models.way['threat'], vulnerability=static_models.way['vulnerability'],
                                  risk=static_models.way['risk'], impact=','.join(static_models.way['impacts']), security_requirement=','.join(static_models.way['requirements']),
                                  counter_measure=','.join(static_models.way['counter']))
    db.session.add(config)
    db.session.commit()
    return app.response_class(response=json.dumps({'message': 'configuration saved', 'status': True}), status=200, mimetype='Application/json')


@app.route("/config", methods=['GET'])
@cross_origin()
def get_configurations():
    data = models.Configuration.query.all()
    res = []
    for d in data:
        print(d.to_dict())
        res.append(d.to_dict())
    return app.response_class(response=json.dumps({'items': res}), status=200, mimetype='Application/json')
