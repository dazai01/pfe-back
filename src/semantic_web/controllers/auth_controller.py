from semantic_web import app, db
from flask import request
from flask_cors import cross_origin
from semantic_web.models.models import User
import uuid
import json
import jwt
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.exceptions import BadRequest


@app.route("/auth/register", methods=['POST'])
@cross_origin()
def register():
    request_body = request.get_json()
    password = generate_password_hash(
        request_body.get('password'), method='sha256')
    user = User(id=uuid.uuid4().hex, name=request_body.get('name'),
                password=password, email=request_body.get('email'))
    db.session.add(user)
    db.session.commit()
    return app.response_class(response=json.dumps({'message': 'user created successfully', 'status': True}), status=201,
                              mimetype='application/json')


@app.route('/auth/login', methods=['POST'])
@cross_origin()
def login():
    login_request = request.get_json()
    if not login_request.get('email') or not login_request.get('password'):
        raise BadRequest('required name and password')

    user = User.query.filter_by(email=login_request.get('email')).first()

    if not user:
        raise BadRequest('user not found with email ' +
                         str(login_request.get('email')))

    if check_password_hash(user.password, login_request['password']):
        token = jwt.encode({'name': user.name, 'email': user.email, 'exp': datetime.utcnow(
        ) + timedelta(days=5)}, app.config.get('SECRET_KEY'), algorithm='HS256')
        user_dict = {'_id': user.id, 'name': user.name, 'email': user.email}
        jwt_token = {'type': 'Bearer', 'accessToken': token, 'expires_in': 5}
        response = {'user': user_dict, 'token': jwt_token}
        return app.response_class(response=json.dumps(response), status=200, mimetype='application/json')

    raise BadRequest('Could not authenticate')
