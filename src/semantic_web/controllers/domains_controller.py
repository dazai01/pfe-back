from semantic_web import app
import json
from flask_cors import cross_origin
from semantic_web.services import ontologies_service, shared
from flask import request
from semantic_web.configuration.config import token_required
from semantic_web.models import static_models


@app.route("/domains", methods=["GET"])
@cross_origin()
@token_required
def getAllDomains(current_user):
    domains = ontologies_service.getAllDomains()
    names = []
    for element in domains:
        names.append(element.name)
    response = shared.map_to_identity(names)
    return app.response_class(response=json.dumps({"items": response}), status=200, mimetype='application/json')


@app.route("/domains/<domain>", methods=["GET"])
@cross_origin()
@token_required
def getSubDomains(current_user, domain):
    domain = request.view_args['domain']
    subDomains = ontologies_service.getSubDomains(domain)
    names = []
    for element in subDomains:
        names.append(element.name)
    static_models.way['domain'] = domain
    response = shared.map_to_identity(names)
    return app.response_class(response=json.dumps({"items": response}), status=200, mimetype='application/json')


@app.route("/domains/assets/<subDomain>", methods=["GET"])
@cross_origin()
# @token_required
def getAssets(subDomain):
    subDomain = request.view_args['subDomain']
    assets = ontologies_service.getAssets(subDomain)
    names = []
    for element in assets:
        names.append(element.name)
    static_models.way['subDomain'] = subDomain
    print("#################")
    print(names)
    response = shared.map_to_identity(names)
    return app.response_class(response=json.dumps({"items": response}), status=200, mimetype='application/json')
