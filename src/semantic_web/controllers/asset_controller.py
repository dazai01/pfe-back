import json
from flask import request
from flask_cors import cross_origin
from semantic_web import app
from semantic_web.services import ontologies_service, shared
from semantic_web.models import static_models
from semantic_web.configuration.config import token_required


@app.route('/assets/threats/<asset>', methods=['GET'])
@cross_origin()
# @token_required
def show_threats(asset):
    asset = request.view_args['asset']
    ontologies_service.addAssetToRiskOntology(asset)
    threats = ontologies_service.getThreats(asset)
    static_models.way['asset'] = asset
    response = shared.map_to_identity(threats)
    return app.response_class(response=json.dumps({'items': response}), status=200, mimetype='Application/json')


@app.route('/assets/vulnerabilities/<threat>', methods=['GET'])
@cross_origin()
def show_vulnerabilities(threat):
    threat = request.view_args['threat']
    vulnerabilities = ontologies_service.getVulnerabilities(threat)
    print(vulnerabilities)
    static_models.way['threat'] = threat
    static_models.phase['vulnerabilities'] = vulnerabilities
    response = shared.map_to_identity(vulnerabilities)
    return app.response_class(response=json.dumps({'items': response}), status=200, mimetype='Application/json')


@app.route('/assets/risks/<vulnerability>', methods=['GET'])
@cross_origin()
def show_risks(vulnerability):
    vulnerability = request.view_args['vulnerability']
    static_models.by_phase['vulnerability'] = vulnerability
    risks = ontologies_service.getRisks(vulnerability)
    static_models.way['vulnerability'] = vulnerability
    static_models.phase['risk'] = risks
    response = shared.map_to_identity(risks)
    return app.response_class(response=json.dumps({'items': response}), status=200, mimetype='Application/json')


@app.route('/assets/impacts/<risk>', methods=['GET'])
@cross_origin()
def show_impacts(risk):
    risk = request.view_args['risk']
    static_models.by_phase['risk'] = risk
    impacts = ontologies_service.getImpacts(risk)
    static_models.way['risk'] = risk
    static_models.way['impacts'] = impacts
    response = shared.map_to_identity(impacts)
    return app.response_class(response=json.dumps({'items': response}), status=200, mimetype='Application/json')
