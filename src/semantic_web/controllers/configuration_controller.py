import json

from flask import request
from semantic_web import app
from semantic_web.services import shared


@app.route('/configuration/examples', methods=['GET'])
def view_examples():
    examples = shared.map_to_identity(app.config.get('EXAMPLES'))
    return app.response_class(response=json.dumps({'items': examples}), status=200, content_type='application/json')


@app.route('/configuration/publish/<example>', methods=['POST'])
def publish_example(example):
    example = request.view_args['example']
    app.config.get('EXAMPLES').append(example)
    return app.response_class(response=json.dumps({'message': 'Configuration published', 'status': True}), status=201, content_type='application/json')
