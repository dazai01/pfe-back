import json
from semantic_web import app
from semantic_web.configuration.config import token_required
from flask_cors import cross_origin


@app.route('/user/me', methods=['GET'])
@cross_origin()
@token_required
def getProfile(current_user):
    output = {'_id': current_user.id, 'name': current_user.name, 'email': current_user.email}
    return app.response_class(response=json.dumps(output), status=200, mimetype='application/json')