from flask import Flask
from flask_cors import CORS

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
cors = CORS(app)
connection_string = "mysql+pymysql://root:pwd@localhost:3306/semantic_web"


# app.config['FCM_SERVER_KEY'] = 'AAAAkuYRvWU:APA91bEdx2hphQq70zPD0ALHure2x5JivuA59f_PkIilbJnHylRvrmvNBJCJ682KCT4-YFEfjfR6JAj14JFHRxv0fZIg0i8daXQ4ScWA3zBmmeWKo54NRm-3rzaBNRGC_Ka9gepdFQjp'
# app.config['FCM_SERVER_URL'] = 'https://fcm.googleapis.com/fcm/send'
app.config['SQLALCHEMY_DATABASE_URI'] = connection_string
app.config['SECRET_KEY'] = '36b65991bdf09eb8f3aefe7a9b838039e0eb8414a86eeb264737d6b3cc6eda70'
app.config['EXAMPLES'] = ['presedential elections', '']
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from semantic_web.controllers import domains_controller, auth_controller, user_controller, asset_controller, configuration_controller, security_requirements_controller