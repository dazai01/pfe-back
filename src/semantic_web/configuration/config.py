from semantic_web import app
from functools import wraps
from flask import request
from datetime import datetime
import jwt
import json
from werkzeug.exceptions import Unauthorized

from semantic_web.models.models import User


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'Authorization' in request.headers:
            token = request.headers.get('Authorization')

        if not token:
            raise Unauthorized('Could not authenticate')

        try:
            data = jwt.decode(token[7:], app.config.get(
                'SECRET_KEY'), algorithms='HS256')
            current_user = User.query.filter_by(
                email=data.get('email')).first()
        except:
            raise Unauthorized('invalid jwt')

        return f(current_user, *args, **kwargs)

    return decorated


@app.errorhandler(Exception)
def handle_exception(e):
    error_response = {'time_stamp': datetime.utcnow().__str__(), 'error': e.name, 'message': e.description,
                      'path': request.path, 'status': e.code}
    return app.response_class(response=json.dumps(error_response), status=e.code, mimetype='application/json')
