from semantic_web import db


class User(db.Model):
    id = db.Column('_id', db.String(256), primary_key=True)
    name = db.Column(db.String(256))
    password = db.Column(db.String(256))
    email = db.Column(db.String(256))


class Configuration(db.Model):
    id = db.Column('_id', db.String(256), primary_key=True)
    domain = db.Column(db.String(256))
    sub_domain = db.Column(db.String(256))
    asset = db.Column(db.String(256))
    threat = db.Column(db.String(256))
    vulnerability = db.Column(db.String(256))
    risk = db.Column(db.String(256))
    impact = db.Column(db.String(256))
    security_requirement = db.Column(db.String(256))
    counter_measure = db.Column(db.String(256))

    def to_dict(self):
        return {
            'domain': self.domain,
            'sub_domain': self.sub_domain,
            'asset': self.asset,
            'threat': self.threat,
            'vulnerability': self.vulnerability,
            'risk': self.risk,
            'impact': self.impact,
            'security_requirement': self.security_requirement,
            'counter_measure': self.counter_measure,
        }