threats : dict = {
    'Ballot': ['Path_Traversal', 'URL_Manipulation'],
    'Voterlist': ['Path_Traversal', 'URL_Manipulation'],
    'Password': ['Phishing', 'Dictionary_Attack', 'Rainbow_Tables', 'Man_In_The_Middle'],
    'CryptographicKeys': ['User_Error1'],
    'TaxData': ['Path_Traversal', 'URL_Manipulation', 'Cross_Site_Request_Forgery', 'SQL_Injection'],
    'UserSession': ['CrossURLscripting'],
    'WebSite': ['DoS-TCP', 'Distributed_Denial_of_Service', 'Cross_Site_Request_Forgery'],
    'WebServer': ['DoS-TCP', 'Distributed_Denial_of_Service'],
    'BankAccount': ['Phishing', 'Cross_Site_Request_Forgery'],
    'PersonalData':  ['Phishing', 'URL_Manipulation', 'SQL_Injection', 'Cross_Site_Request_Forgery'],
    'WebBrowser': ['Cross_Site_Request_Forgery'],
    'LoginName': ['SQL_Injection'],
    'SessionID': ['Dictionary_Attack', 'Rainbow_Tables', 'Session_Fixation'],
    'ApplicationServer': ['SQL_Injection'],
    'DBFile': ['SQL_Injection'],
    'FlightData': ['Phishing'],
    'PatientFile': ['URL_Manipulation', 'Path_Traversal', 'SQL_Injection'],
    'PatientSurgicalFile': [],
    'MedicalAppointment': ['URL_Manipulation', 'Path_Traversal', 'Cross_Site_Request_Forgery'],
    'MedicalData': []
}

security_requirements : dict = {
    'unvalidated_URL_redirection_to_untrusted_site' : {
        'Gain_Privileges': ['authenticate_communicating_party', 'protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'use_input_validation_framework', 'semantic_input_validation', 'syntactical_input_validation'],
        'Password_Disclosure': ['use_input_validation_framework', 'protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'semantic_input_validation', 'syntactical_input_validation'],
        'Data_Disclosure': ['use_input_validation_framework', 'protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'semantic_input_validation', 'syntactical_input_validation'],
        'Access_Unauthorized_Data': ['protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'use_input_validation_framework', 'semantic_input_validation', 'syntactical_input_validation'],
        'Install_malware': ['protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'use_input_validation_framework'],
        'Redirect_user_To_Malicious_Site': ['protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'use_input_validation_framework', 'semantic_input_validation', 'syntactical_input_validation'],
        'Access_Control_Bypass': ['Use a list of approved URLs'],
        'Credential_Theft': ['protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'use_input_validation_framework', 'semantic_input_validation', 'syntactical_input_validation'],
        'Identity_Theft': ['protect_user/url_redirection', 'prevent_URL_redirection_to_untrusted_site', 'use_input_validation_framework', 'semantic_input_validation', 'syntactical_input_validation']
        },
    'Sensitive_Cookies_Without_Http_Only_Flag': {
        'Gain_Privileges': ['secure_storage', 'avoid_storing_sensitive_information_in_persistant_cookies', 'prevent_information_exposure_throught_persistant_cookies', 'protect_sensitive_cookies']
    },
    'Improper_Enforcement_Of_Message_Integrity': {
        'Gain_Privileges': ['auditing transactions', 'need_integrity', 'reliable_communication', 'enforcement_of_message_integrity_during_transmission_in_communication_channel']
    },
    'Unverified_Input': {
        'Gain_Privileges': ['semantic_input_validation', 'file_upload_validation', 'automatic_boundary_Checking', 'measure_to_injection', 'specify_sufficient_number_of_buffers', 'use_input_validation_framework', 'file_upload_validation', 'implement_bounds_checking_on_input', 'syntactical_input_validation']
    },
    'Reliance_on_IP_Address_for_authentication': {
        'Gain_Privileges': ['need_access_control', 'authenticate_communicating_party']
    },
    'Insufficient_Verification_Of_Identity': {
        'Gain_Privileges': ['semantic_input_validation', 'authenticate_communicating_party', 'need_access_control', 'ensure_usernames/userids_security']
    },
    'Unsecure_Criptographic_Storage': {
        'Gain_Privileges': ['avoid_obsolete_cryptography', 'store_passwords_in_a_secure_fashion', 'secure_storage']
    },
    'Overly_Informative_Error_Messages': {
        'Gain_Privileges': ['Prevent_information_exposure_throught_error_message']
    },
    'Information_Exposure_Through_Error_Message': {
        'Gain_Privileges': ['Prevent_information_exposure_throught_error_message']
    },
    'Using_referer_field_for_authentication': {
        'Gain_Privileges': ['semantic_input_validation', 'file_upload_validation', 'authenticate_communicating_party']
    },
    'Weak_Password_Recovery_Mechanism': {
        'Gain_Privileges': ['reinforce_password_recovery_mechanism']
    },
    'Failure_To_Restrict_URL_Access': {
        'Access_Private_Functions': ['restrict_URL_access', 'enforce_access_control_mechanism_at_the_server_side', 'Restrict_function_level_access'],
        'Access_Unprotected_Files': ['restrict_URL_access', 'protect_accessible_object', 'use_possible_permission_on_file_access'],
        'Access_Unauthorized_Data': ['restrict_URL_access', 'protect_accessible_object']
    },
    'Insufficient_Verification_Of_Identity': {
        'Access_Control_Bypass': ['need_access_control', 'need_integrity', 'protecting_session_hijacking', 'reliable_communication', 'Sufficient_verification_of_identity_at_both_ends_of_communication_channel', 'Identity_verfication_by_username/password'],
        'Identity_Theft': ['protecting_session_hijacking', 'need_access_control', 'need_integrity', 'reliable_communication', 'Sufficient_verification_of_identity_at_both_ends_of_communication_channel', 'Identity_verfication_by_username/password']
    },
    'Reliance_on_IP_Address_for_authentication': {
        'Access_Control_Bypass': ['need_access_control', 'IP_address_verification', 'Identity_verfication_by_username/password', 'Identity_verfication_by_username/password']
    },
    'Improper_Enforcement_Of_Message_Integrity': {
        'Data_Tampering': ['reliable_communication', 'enforcement_of_message_integrity_during_transmission_in_communication_channel'],
        'Gain_Privileges': ['reliable_communication', 'enforcement_of_message_integrity_during_transmission_in_communication_channel']
    },
    'default': {
        'Gain_Privileges': ['semantic_input_validation', 'syntactical_input_validation'],
        'Credential_Theft': ['user_authentication'],
        'Password_Disclosure': ['user_authentication', 'enhancement_of_password'],
        'Access_Unauthorized_Data': ['need_access_control'],
        'Data_Manipulation': ['need_confidentiality'],
        'Accessing_Account_Information': ['syntactical_input_validation', 'protect_accessible_object', 'need_encryption_to_store_private_data', 'semantic_input_validation'],
        'Access_Unauthorized_Page': ['protect_accessible_object'],
        'Access_Private_Functions': ['enforce_access_control_mechanism_at_the_server_side']
    }
}


by_phase: dict = {
    'vulnerability': '',
    'risk': '',
    'requirements': '',
    'design': '',
    'analysis': '',
    'implementation': ''
}

phase: dict = {}

way: dict = {}

vulnerability_type: dict = {
    'Security_Misconfiguration': 'analysis',
    'VulnerabilityInAnalysis': 'analysis',
    'Improper_Input_Validation': 'implementation',
    'Misused_File_System': 'implementation',
    'Misused_Privilege_Management': 'implementation',
    'Sensitive_Data_Exposure': 'implementation',
    'VulnerabilityInCode': 'implementation',
    'Improper_Access_Control': 'design',
    'Improper_Authentication': 'design',
    'Channel_Accessible_By_Non-Endpoint': 'design',
    'Weak_Account_Management_Functions': 'design',
    'Improper_Authorization': 'design',
    'Insufficient_Verification_Of_Data_Athenticity': 'design',
    'Missing_Function_Level_Access_Control': 'design',
    'Not_using_input_validation_framework': 'design',
    'Unprotected_bounds_of_buffer': 'design',
    'Unvalidated_redirects': 'design',
    'VulnerabilityInDesign': 'design',
}

req_type: dict = {
    'Secure_analyse': 'analysis',
    'Secure_design': 'design',
    'Enforce_account_management_functions': 'design',
    'Protect_bounds_of_buffer': 'design',
    'Protect_sensitive_information_storage': 'design',
    'Protect_sensitive_information_transmission': 'design',
    'Protect_usernames/userids': 'design',
    'Restrict_function_level_access': 'design',
    'Sufficient_verification_of_identity_at_both_ends_of_communication_channel': 'design',
    'Validate_redirection': 'design',
    'Secure_implementation': 'implementation',
    'Code_checking': 'implementation',
    'Escape_untrusted_data': 'implementation',
    'Input_validation': 'implementation',
    'Memory_protection': 'implementation',
    'Network_configuration': 'implementation',
    'Prevent_information_exposure': 'implementation',
    'Encrypting_sensitive_data': 'implementation',
    'Verify_authorization_to_system_object': 'implementation'
}
